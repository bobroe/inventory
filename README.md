SYSTEM INSTALL AND START

This source code is loopback.io project. To deploy this project please install 
loopback.io, npm and node.js on your server and then:

1. Install modules with: npm install
2. Start loopback: node . or using pm2 start

That's it.

MAKING QUERIES

Look at http://loopback.io/doc/en/lb3/index.html please.
All documantation for this API is: your_site.com/explorer/#/ (your domain 
and port may be different according your server setup).

Example query to get all currencies:
curl -X GET --header 'Accept: application/json' 'http://your_site.com/api/InvCurrencies' 
or just http://your_site.com/api/InvCurrencies

Example query with loopback.io mighty filters to get only USD (by currency name, not just id):
http://your_site.com/api/InvCurrencies?filter={"where":{"invCurrencyCode":"USD"}}