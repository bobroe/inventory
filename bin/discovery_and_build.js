var path = require('path');
var fs = require('fs');
//var app = require(path.resolve(__dirname, '../server'));
var loopback = require('loopback');
var app = loopback();
var outputPath = path.resolve('/var/www/inventory/common/models');

//var dataSource = app.dataSource.inventory;
var dataSource = loopback.createDataSource({
  connector: 'mysql',
  host: 'localhost',
  database: 'inventory',
  username: 'root',
  password: 'toor'
});


function schemaCB(err, schema) {
  if(schema) {
    //var tbl = schema.name;
    console.log("Auto discovery success: " + schema.name);
    var outputName = outputPath + '/' +schema.name + '.json';
    var outputNameJs = outputPath + '/' +schema.name + '.js';
    var outputTextJs = "'use strict';module.exports = function(" + schema.name  +") {};";
    fs.writeFile(outputName, JSON.stringify(schema, null, 2), function(err) {
      if(err) {
        console.log(err);
        return;
      } else {
        console.log("JSON saved to " + outputName);
      }
    });

    fs.writeFile(outputNameJs, outputTextJs, function(err) {
      if(err) {
        console.log(err);
        return;
      } else {
        console.log("JS saved to " + outputNameJs);
      }
    });

  }
  if(err) {
    console.error(err);
    return;
  }
  return;
};

dataSource.discoverSchema('inv_order',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_i18n_string',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_location',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_product_has_inv_product_cat',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_stock',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_currency',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_image',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_product_has_inv_recepie',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_stock_line',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_customer',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_inventory',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_order_line',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_product_has_inv_supplier',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_supplier',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_customer_has_wp_users',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_inventory_line',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_product',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_recepie',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('wp_users',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_i18n_entity',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_inventory_units',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_product_cat',{schema:'inventory'},schemaCB);
dataSource.discoverSchema('inv_recepie_cat',{schema:'inventory'},schemaCB);

